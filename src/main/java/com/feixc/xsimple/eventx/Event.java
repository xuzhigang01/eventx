package com.feixc.xsimple.eventx;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解：事件。用于事件处理方法<br>
 * 用法： Event(value = "事件名", priority = EventPriority.XXX)
 * 
 * @author xuzhigang01
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Event {
	String value() default "";

	int priority() default EventPriority.HIGH;
}
