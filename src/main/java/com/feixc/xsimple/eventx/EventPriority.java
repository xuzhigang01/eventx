package com.feixc.xsimple.eventx;

/**
 * 事件优先级
 * 
 * @author xuzhigang01
 */
public class EventPriority {
	public static final int HIGH = 9;
	public static final int MIDDLE = 5;
	public static final int LOW = 1;
}
