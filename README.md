<pre>
xsimple: 致力于开发一些简单的java程序组件。遵循：简单即是美！

eventx: 极简的java异步事件处理组件，使用优先级队列线程池。特点：简单、易于使用。
可为事件设置优先级，处理完一个任务即提升等待任务的优先级，在任务优先级与创建时间中取得平衡。

主要组成部分：
发送事件类--EventX
事件分发器--EventDispatcher
事件处理器--EventHandler

使用流程：
1. 引入jar
&lt;dependency&gt;
    &lt;groupId&gt;com.feixc.xsimple&lt;/groupId&gt;
    &lt;artifactId&gt;eventx&lt;/artifactId&gt;
    &lt;version&gt;1.0.0&lt;/version&gt;
&lt;/dependency&gt;

2. 编写事件处理代码
package xxx.yyy.zzz;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.feixc.xsimple.eventx.BaseEventHandler;
import com.feixc.xsimple.eventx.Event;
import com.feixc.xsimple.eventx.EventHandler;
import com.feixc.xsimple.eventx.EventPriority;

@EventHandler("/test")
public class TestHandler extends BaseEventHandler {

    @Event("aa")
    public void aa(Map<String, Object> params) {
		String a = getParam(params, "a", String.class, true);
		int b = getParam(params, "b", Integer.class, true);
		Long c = getParam(params, "c", Long.class, false);
		Boolean d = getParam(params, "d", Boolean.class, false);
		
		// do something
	}

	@Event(value = "bb", priority = EventPriority.MIDDLE)
	public void bb(Map<String, Object> params) {
		// do something
	}

	@Event(value = "cc", priority = EventPriority.LOW)
	public void cc(Map<String, Object> params) {
		// do something
	}
}

3. 初始化事件分发器--EventDispatcher
在代码中初始化（只需初始化一次），参数为事件处理器类包名：
EventDispatcher.init("xxx.yyy.zzz")

或在spring配置文件中初始化：
&lt;bean id="eventDispatcher" class="org.opensource.xsimple.eventx.EventDispatcher"&gt;
    &lt;constructor name="handlerPackage" value="xxx.yyy.zzz" /&gt;
&lt;/bean&gt;

4. 在代码中发送事件
Map<String, Object> params = new HashMap<String, Object>();
params.put("a", "aaaaaaaaaa");
params.put("b", 1234);
params.put("c", 0);
params.put("d", true);

EventX.sendEvent("/test/aa", params);
</pre>